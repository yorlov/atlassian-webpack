const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');

const ROOT_DIR = path.resolve(__dirname);
const FRONTEND_DIR = path.join(ROOT_DIR, 'src')
const OUTPUT_DIR = path.join(ROOT_DIR, 'target', 'classes');
const DESCRIPTORS_PATH = path.join(OUTPUT_DIR, 'META-INF', 'plugin-descriptors', 'bundles.xml')

module.exports = {
    devtool: 'source-map',
    target: ['es5'],
    context: FRONTEND_DIR,
    entry: {
        feature: path.join(FRONTEND_DIR, 'feature.js'),
    },
    output: {
        filename: '[name].js',
        libraryTarget: 'amd',
        library: 'bitbucket-plugin-test/[name]',
        path: OUTPUT_DIR
    },
    plugins: [
        new WrmPlugin({
            pluginKey: 'com.stiltsoft.bitbucket.test',
            transformationMap: {
                js: ['jsI18n']
            },
            xmlDescriptors: DESCRIPTORS_PATH,
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css'
        })
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {loader: MiniCssExtractPlugin.loader},
                    {loader: 'css-loader'},
                ]
            },
            {
                test: /\.png$/,
                use: [
                    {loader: 'file-loader', options: {name: '[name].[ext]'}}
                ]
            }
        ],
    }
};
